# Ac-255 Synthesis

Basic simulation for Ac-225 synthesis using a 78-192 MeV proton beam on a Th-232 target

## Analysis

Detailed analysis can be found here: https://docs.google.com/document/d/1PuQmX0vs00iKnbbhAGJGMSKBw6yOs9RcPan2oIbn_qA

## Usage

After setting up Geant4, do:

```bash
mkdir build && cd build
cmake ..
make -j4
./sim
```

For running simulation several times, use a macro: `./sim run.mac`.
Data will be saved to a root file which can then be analyzed using root:
```bash
root output0.root
```

In root, you can for example use `TBrowser` to quickly study histograms:
```c++
new TBrowser
```

Plot correlation histograms for energy penetration:
```c++
Energy->Draw("fEnergy:fZ", "", "colz")
```

Zoom into proton beam:
```c++
Energy->Draw("fEnergy:fZ", "fEnergy > 30", "colz")
```

## Compatability

This has been tested to work with geant4-v11.0.2 on Linux and 8-core 8GB machine.

## Resources

Harvey, James, et al. "Production of Actinium-225 via High Energy Proton Induced Spallation of Thorium-232." Applications Of High Intensity Proton Accelerators. 2010. 321-326. https://www.osti.gov/servlets/purl/1032445

Has description of target dimensions, and analysis of target heating
along with particle distribution.

## Acknowledgements
* Geant4 Project Template from https://github.com/MustafaSchmidt/geant4-tutorial
