#include "event.hh"

MyEventAction::MyEventAction(MyRunAction*)
{
  fEdep = 0.;
}

MyEventAction::~MyEventAction()
{}

void MyEventAction::BeginOfEventAction(const G4Event*)
{
  fEdep = 0.;
}

void MyEventAction::EndOfEventAction(const G4Event*)
{
  //G4cout << "Energy deposition: " << fEdep << G4endl;

  G4AnalysisManager *man = G4AnalysisManager::Instance();

  man->FillNtupleDColumn(1, 0, fEdep);

  man->AddNtupleRow(1);
}

void MyEventAction::ProcessStep(const G4Step *step)
{
  G4StepPoint *preStepPoint = step->GetPreStepPoint();
  G4int evt = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4ThreeVector pos = preStepPoint->GetPosition();
  G4ThreeVector mom = preStepPoint->GetMomentum();
  G4double edep = step->GetTotalEnergyDeposit();

  G4AnalysisManager *man = G4AnalysisManager::Instance();

  man->FillNtupleIColumn(0, 0, evt);
  man->FillNtupleDColumn(0, 1, pos[0]);
  man->FillNtupleDColumn(0, 2, pos[1]);
  man->FillNtupleDColumn(0, 3, pos[2]);
  man->FillNtupleDColumn(0, 4, mom.mag());
  man->FillNtupleDColumn(0, 5, edep);
  
  man->AddNtupleRow(0);
 
  /*
  G4cout
    << evt
    << " Pos: " << pos
    << " Mom: " << mom.mag()
    << " Edep: " << edep
    << G4endl;
  */

  fEdep += edep;
}
