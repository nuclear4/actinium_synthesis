#ifndef CONSTRUCTION_HH
#define CONSTRUCTION_HH

#include "G4VUserDetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4GenericMessenger.hh"

class MyDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    MyDetectorConstruction();
    ~MyDetectorConstruction();

    G4LogicalVolume *GetScoringVolume() const { return fScoringVolume; }

    virtual G4VPhysicalVolume *Construct();

  private:
    G4Box *solidWorld, *solidRadiator;
    G4LogicalVolume *logicWorld, *logicRadiator;
    G4VPhysicalVolume *physWorld, *physRadiator;

    G4LogicalVolume *fScoringVolume;

    G4Material *metalTh, *worldMat;

    void DefineMaterials();
};

#endif
