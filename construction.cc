#include "construction.hh"

MyDetectorConstruction::MyDetectorConstruction()
{
  DefineMaterials();
}

MyDetectorConstruction::~MyDetectorConstruction()
{}

void MyDetectorConstruction::DefineMaterials()
{
  G4NistManager *nist = G4NistManager::Instance();
  
  G4double energy[2] = {1.239841939*eV/0.9, 1.239841939*eV/0.2};
  G4double rindexWorld[2] = {1.0, 1.0};

  metalTh = new G4Material("Th Metal", 11.7*g/cm3, 1);
  metalTh->AddElement(nist->FindOrBuildElement("Th"), 1);
  
  worldMat = nist->FindOrBuildMaterial("G4_AIR");

  G4MaterialPropertiesTable *mptWorld = new G4MaterialPropertiesTable();
  mptWorld->AddProperty("RINDEX", energy, rindexWorld, 2);
  worldMat->SetMaterialPropertiesTable(mptWorld);
}

G4VPhysicalVolume *MyDetectorConstruction::Construct()
{
  G4double xWorld = 0.5*m;
  G4double yWorld = 0.5*m;
  G4double zWorld = 0.5*m;

  solidWorld = new G4Box("solidWorld", xWorld, yWorld, zWorld);

  logicWorld = new G4LogicalVolume(solidWorld, worldMat,
      "logicWorld");

  physWorld = new G4PVPlacement(0,
      G4ThreeVector(0., 0., 0.), logicWorld, "physWorld", 0, false, 0, true);

  solidRadiator = new G4Box("solidRadiator", 1*cm, 1*cm, 1*cm);

  logicRadiator = new G4LogicalVolume(solidRadiator,
      metalTh, "logicalRadiator");

  fScoringVolume = logicRadiator;

  physRadiator = new G4PVPlacement(0,
      G4ThreeVector(0., 0., 0.25*m), logicRadiator, "physRadiator",
      logicWorld, false, 0, true);

  return physWorld;
}
